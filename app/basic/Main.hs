module Main where

import System.IO

sayHello :: String -> IO ()
sayHello name =
    putStrLn ("Hi " ++ name ++ "!")

main :: IO ()
main = do
    -- prevent putStr from buffering and dump it to stdout immediately
    hSetBuffering stdout NoBuffering
    putStr "Please input your name: "
    name <- getLine
    sayHello name
