module Main where

import Control.Monad (forever)
import Data.Char (toLower)
import Data.Maybe (isJust)
import Data.List (intersperse)
import System.Exit (exitSuccess)
import System.Random (randomRIO)

import Puzzle

-- game

main :: IO ()
main = do
    word <- randomWord'
    let puzzle = freshPuzzle (fmap toLower word)
    runGame puzzle

handleGuess :: Puzzle -> Char -> IO Puzzle
handleGuess puzzle guess = do
    putStrLn $ "Your guess was: " ++ [guess]
    case (charInWord puzzle guess, alreadyGuessed puzzle guess) of
        (_, True) -> do
            putStrLn "You already guessed this character"
            return puzzle
        (True, _) -> do
            putStrLn "Nice guess, found"
            return (fillInCharacter puzzle guess)
        (False, _) -> do
            putStrLn "Not in the word, sorry"
            return (fillInCharacter puzzle guess)

gameOver :: Puzzle -> IO ()
gameOver (Puzzle word _ guesses) =
    if (length guesses) > 15 then
        do putStrLn "You lose!"
           putStrLn $ "The word was" ++ word
           exitSuccess
    else return ()

gameWin :: Puzzle -> IO ()
gameWin (Puzzle _ filled _) =
    if all isJust filled then
        do putStrLn "You win!"
           exitSuccess
    else return ()

runGame :: Puzzle -> IO ()
runGame puzzle = forever $ do
    gameOver puzzle
    gameWin puzzle
    putStrLn $ "Current puzzle is: " ++ show puzzle
    putStr "Guess a letter: "
    guess <- getLine
    case guess of
        [c] -> handleGuess puzzle c >>= runGame
        _ -> putStrLn "Your guess must be a single character"

-- word list

newtype WordList = Wordlist [String]

minWordLength :: Int
minWordLength = 5

maxWordLength :: Int
maxWordLength = 9

allWords :: IO WordList
allWords = do
    dict <- readFile "/usr/share/dict/words"
    return $ Wordlist (lines dict)

gameWords :: IO WordList
gameWords = do
    (Wordlist aw) <- allWords
    return $ Wordlist (filter valid aw)
    where gameLength w =
            let l = length (w :: String) in
                l >= minWordLength && l <= maxWordLength
          valid w = gameLength w && all (/='\'') w

randomWord :: WordList -> IO String
randomWord (Wordlist ws) = do
    randomIdx <- randomRIO (0, length ws - 1)
    return $ ws !! randomIdx

randomWord' :: IO String
randomWord' = gameWords >>= randomWord
