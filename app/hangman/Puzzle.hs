module Puzzle where

import Data.List (intersperse)

data Puzzle =
    Puzzle String [Maybe Char] [Char]

instance Show Puzzle where
    show (Puzzle _ discovered guessed) =
        (intersperse ' ' $
         fmap renderPuzzleChar discovered)
        ++ " Missed so far: " ++ guessed

freshPuzzle :: String -> Puzzle
freshPuzzle word = Puzzle word hidden [] where
    hidden = map (const Nothing) word

charInWord :: Puzzle -> Char -> Bool
charInWord (Puzzle w _ _) c = elem c w

alreadyGuessed :: Puzzle -> Char -> Bool
alreadyGuessed (Puzzle _ _ gs) c = elem c gs

fillInCharacter :: Puzzle -> Char -> Puzzle
fillInCharacter p@(Puzzle w filled guesses) guess =
    Puzzle w nextFilled nextGuesses where
        nextFilled = zipWith zipper w filled
        -- combine a character in the word with a
        -- character position filled by player
        zipper wordChar filledChar =
            if wordChar == guess
            then Just guess
            else filledChar
        nextGuesses = if charInWord p guess
            then guesses
            else guess:guesses

renderPuzzleChar :: Maybe Char -> Char
renderPuzzleChar Nothing = '_'
renderPuzzleChar (Just c) = c
