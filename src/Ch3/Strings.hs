module Ch3.Strings where

printSmth :: IO ()
printSmth = putStrLn "sraka"

printSmthElse :: IO ()
printSmthElse = do
    putStrLn "aaaa"
    putStrLn "bbb"

s1 :: String
s1 = "sraka1"

s2 :: String
s2 = "sraka2"

s3 :: String
s3 = s1 ++ s2

printConcat :: IO ()
printConcat = do
    putStrLn "a"
    putStrLn b
    where b =
            concat [s1, " ", s2]

-- exersizes

bangify :: String -> String
bangify s = s ++ "!"

someLetter :: String -> String
someLetter s = take 1 (drop 4 s)

someChunk :: String -> String
someChunk s = drop 9 s
-- can also be written as someChunk = drop 9

thirdLetter :: String -> Char
thirdLetter s = s !! 2

nThLetter :: String -> Int -> Char
nThLetter s n = s !! n

-- take substring of a string, starting from index n inclusive, and up to len characters
substr :: String -> Int -> Int -> String
substr s n len = take len (drop n s)

curryStr :: String
curryStr = "Curry is awesome"
rvrsCurry :: String
rvrsCurry = (substr curryStr 9 7) ++ (substr curryStr 5 4) ++ (take 5 curryStr)