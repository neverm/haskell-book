module Ch15.Tasks where

import Test.QuickCheck

-- 1
data Trivial = Trivial deriving (Eq, Show)

instance Semigroup Trivial where
  _ <> _ = Trivial

instance Arbitrary Trivial where
  arbitrary = return Trivial

semigroupAssoc :: (Eq m, Semigroup m)
               => m -> m -> m -> Bool
semigroupAssoc a b c =
  (a <> (b <> c)) == ((a <> b) <> c)

-- 2
newtype Identity a = Identity a deriving (Eq, Show)

instance Semigroup a => Semigroup (Identity a) where
  (Identity a) <> (Identity b) = Identity $ a <> b

instance Arbitrary a => Arbitrary (Identity a) where
  arbitrary = do
    a <- arbitrary
    return $ Identity a

-- 3
data Two a b = Two a b deriving (Eq, Show)

instance (Semigroup a, Semigroup b) => Semigroup (Two a b) where
  (Two a b) <> (Two a' b') = Two (a <> a') (b <> b')

instance (Arbitrary a, Arbitrary b) => Arbitrary (Two a b) where
  arbitrary = do
    a <- arbitrary
    b <- arbitrary
    return $ Two a b

-- 4
data Three a b c = Three a b c deriving (Eq, Show)

instance (Semigroup a, Semigroup b, Semigroup c) => Semigroup (Three a b c) where
  (Three a b c) <> (Three a' b' c') = Three (a <> a') (b <> b') (c <> c')

instance (Arbitrary a, Arbitrary b, Arbitrary c) => Arbitrary (Three a b c) where
  arbitrary = do
    a <- arbitrary
    b <- arbitrary
    c <- arbitrary
    return $ Three a b c

-- 5 same shit, omitting

-- 6
newtype BoolConj = BoolConj Bool deriving (Eq, Show)

instance Semigroup BoolConj where
  BoolConj False <> _ = BoolConj False
  _ <> BoolConj False = BoolConj False
  _ <> _ = BoolConj True

instance Arbitrary BoolConj where
  arbitrary = do
    b <- arbitrary
    return $ BoolConj b

-- 7
newtype BoolDisj = BoolDisj Bool deriving (Eq, Show)

instance Semigroup BoolDisj where
  BoolDisj True <> _ = BoolDisj True
  _ <> BoolDisj True = BoolDisj True
  _ <> _ = BoolDisj False

instance Arbitrary BoolDisj where
  arbitrary = do
    b <- arbitrary
    return $ BoolDisj b

-- 8
data Or a b = Fst a | Snd b deriving (Eq, Show)

instance Semigroup (Or a b) where
  Snd b <> _ = Snd b
  _ <> Snd b = Snd b
  Fst a <> _ = Fst a

instance (Arbitrary a, Arbitrary b) => Arbitrary (Or a b) where
  arbitrary = do
    a <- arbitrary
    b <- arbitrary
    frequency [(1, return $ Fst a), (1, return $ Snd b)]

-- 9
newtype Combine a b =
  Combine { unCombine :: (a -> b)}

instance Semigroup b => Semigroup (Combine a b) where
  Combine f <> Combine g = Combine $ \a -> (g a) <> (f a)

-- 10
newtype Comp a =
  Comp { unComp :: (a -> a)}

instance Semigroup (Comp a) where
  Comp f <> Comp g = Comp $ f . g

-- 11
data Validation a b = Fail a | Succ b deriving (Eq, Show)

instance Semigroup a => Semigroup (Validation a b) where
  (Fail a) <> (Fail a') = Fail (a <> a')
  Succ b <> _ = Succ b
  _ <> Succ b = Succ b


-- Monoids

instance Monoid Trivial where
  mempty = Trivial

instance Monoid a => Monoid (Identity a) where
  mempty = Identity mempty

instance (Monoid a, Monoid b) => Monoid (Two a b) where
  mempty = Two mempty mempty

instance Monoid BoolConj where
  mempty = BoolConj True

instance Monoid BoolDisj where
  mempty = BoolDisj False

instance Monoid b => Monoid (Combine a b) where
  mempty = Combine $ \n -> mempty

instance Monoid (Comp a) where
  mempty = Comp id

newtype Mem s a =
  Mem {
    runMem :: s -> (a, s)
  }

instance Semigroup a => Semigroup (Mem s a) where
  (Mem f) <> (Mem f') =
    Mem $ \s -> let
      (a, s') = f s
      (a', s'') = f' s'
    in
    (a <> a', s'')

instance Monoid a => Monoid (Mem s a) where
  mempty = Mem $ \s -> (mempty, s)

f' = Mem $ \s -> ("hi", s + 1)

runMemTest :: IO ()
runMemTest = do
  let zero = runMem mempty 0
      left = runMem (f' <> mempty) 0
      right = runMem (mempty <> f') 0
  print $ left
  print $ right
  print $ (zero :: (String, Int))
  print $ left == runMem f' 0
  print $ right == runMem f' 0
