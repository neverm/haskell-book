module Ch15.Opt where

import Data.Monoid

data Opt a
  = Nada
  | Only a
  deriving (Show, Eq)

instance Semigroup a => Semigroup (Opt a) where
  (Only a) <> (Only b) = Only (a <> b)
  (Only a) <> Nada = (Only a)
  Nada <> (Only a) = (Only a)
  _ <> _ = Nada

instance Monoid a => Monoid (Opt a) where
  mempty = Nada
  mappend = (<>)
  