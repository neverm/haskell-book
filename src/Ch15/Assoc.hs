module Ch15.Assoc where

import Data.Monoid
import Data.Maybe
import Ch15.Opt
import Test.QuickCheck

monoidAssoc :: (Eq m, Monoid m)
            => m -> m -> m -> Bool

monoidAssoc a b c =
  (a <> (b <> c)) == ((a <> b) <> c)

monoidLeftId :: (Eq m, Monoid m)
             => m
             -> Bool

monoidLeftId a = (mempty <> a) == a

monoidRightId :: (Eq m, Monoid m)
              => m
              -> Bool

monoidRightId a = (a <> mempty) == a

data Bull
  = Twoo
  | Fawse
  deriving (Eq, Show)

instance Semigroup Bull where
  a <> b = Fawse

instance Monoid Bull where
  mempty = Fawse
  mappend = (<>)

newtype First' a =
  First' { getFirst' :: Opt a }
  deriving (Eq, Show)

instance Semigroup (First' a) where
  (First' (Only a)) <> _ = (First' (Only a))
  _ <> (First' (Only a)) = (First' (Only a))
  (First' Nada) <> _ = First' Nada

instance Monoid (First' a) where
  mempty = First' Nada

instance Arbitrary Bull where
  arbitrary =
    frequency [ (1, return Fawse)
              , (1, return Twoo)
              ]

instance Arbitrary a => Arbitrary (First' a) where
  arbitrary = do
    a <- arbitrary
    frequency [ (1, return (First' $ Only a))
              , (1, return (First' Nada))
              ]