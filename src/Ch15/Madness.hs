module Ch15.Madness where

import Data.Monoid

type Verb = String
type Adjective = String
type Adverb = String
type Noun = String
type Exclamation = String

madlibbinBetter' :: Exclamation
  -> Adverb
  -> Noun
  -> Adjective
  -> String
madlibbinBetter' e adv noun adj = mconcat lst where
  lst = [e, "! he said ", adv, " as he jumped into his car ",
         noun, " and drove off with his ",
         adj, " wife."
        ]