module Ch7.Pattern where

newtype Username = Username String

newtype AccountNumer = AccountNumer Integer

data User = UnregisteredUser | RegisteredUser Username AccountNumer

printUser :: User -> IO ()
printUser UnregisteredUser =
    putStrLn "Unregistered user"
printUser (RegisteredUser
           (Username name)
           (AccountNumer num)) =
    putStrLn $ name ++ " " ++ show num

-- exercises
f :: (a, b, c)
  -> (d, e, f)
  -> ((a, d), (c, f))
f (a, _, c) (d, _, f) = ((a, d), (c, f))


functionC :: Ord a => a -> a -> a
functionC x y = case (x > y) of
    True  -> x
    False -> y

ifEvenAdd2 x = case even x of
    True  -> x + 2
    False -> x

nums :: (Ord a, Num a) => a -> a
nums x = case compare x 0 of
    LT -> -1
    EQ -> 0
    GT -> 1


-- examples

data Employee = Coder
              | Manager
              | Veep
              | CEO deriving (Eq, Show, Ord)

printBoss :: Employee -> Employee -> IO ()
printBoss e e' = putStrLn report where
    report = show e ++ " is the boss of " ++ show e'

codersRule :: Employee -> Employee -> Ordering
codersRule Coder Coder = EQ
codersRule Coder _ = GT
codersRule _ Coder = LT
codersRule e e' = compare e e'

reportBoss :: (Employee -> Employee -> Ordering)
            -> Employee
            -> Employee
            -> IO ()
reportBoss f e e' =
    case f e e' of
        GT -> printBoss e e'
        EQ -> putStrLn "neither is the boss"
        LT -> (flip printBoss) e e'

-- exercises

tensDigit :: Integral a => a -> a
tensDigit x = d `mod` 10
    where (d, _) = x `divMod` 10

hunsD x = huns `mod` 10
    where (huns, _) = tens `divMod` 10
          tens = x `div` 10

foldBool :: a -> a -> Bool -> a
foldBool c a p = if p then c else a

foldBool2 :: a -> a -> Bool -> a
foldBool2 c a p =
    case p of
        True -> c
        False -> a

foldBool3 :: a -> a -> Bool -> a
foldBool3 c a p
    | p = c
    | otherwise = a

g :: (a -> b) -> (a, c) -> (b, c)
g f (a, c) = (b, c)
    where b = f a

roundTrip :: (Show a, Read a) => a -> a
roundTrip = read . show


roundTrip2 :: (Show a, Read b) => a -> b
roundTrip2 = read . show