module Ch5.Types where

f :: Int -> Int
f x = x + 10

g :: Num a => a -> a
g x = x + 15


-- exercises

f1 :: a -> a -> a -> a
f1 = undefined

f2 :: a -> b -> c -> b
f2 = undefined

h :: (Num a, Num b) => a -> b -> b
h = undefined

jackal :: (Ord a, Eq b) => a -> b -> a
jackal = undefined

kessel :: (Ord a, Num b) => a -> b -> a
kessel = undefined

-- type inference

-- examples

-- f3 :: Num a => a -> a -> a
f3 x y = x + y + 3


-- exercises

myConcat x = x ++ " yo"

myMult x = (x / 3) * 5

myTake x = take x "hey you"

myCom x = x < (length [1..10])

myAlph x = x < 'z'


-- end exercises

ff :: Int -> String
ff = undefined
gg :: String -> Char
gg = undefined
hh :: Int -> Char
hh x = gg $ ff x


data A
data B
data C
q :: A -> B
q = undefined
w :: B -> C
w = undefined
e :: A -> C
e x = w $ q x


data X
data Y
data Z
xz :: X -> Z
xz = undefined
yz :: Y -> Z
yz = undefined
xform :: (X, Y) -> (Z, Z)
xform (a, b) = (xz a, yz b)


munge :: (x -> y)
      -> (y -> (w, z))
      -> x
      -> w
munge f g a = w
    where (w, _) = g $ f a


convList :: a -> [a]
convList x = x : (x :[])