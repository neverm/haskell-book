module Ch9.Lists where

import Data.List (intersperse)

eftBool :: Bool -> Bool -> [Bool]
eftBool from to
    | from > to = []
    | from == to = [to]
    | otherwise = from : eftBool (succ from) to

eftOrd :: Ordering -> Ordering -> [Ordering]
eftOrd from to
    | from > to = []
    | from == to = [to]
    | otherwise = from : eftOrd (succ from) to

eftInt :: Int -> Int -> [Int]
eftInt from to
    | from > to = []
    | otherwise = from : eftInt (succ from) to

eftChar :: Char -> Char -> [Char]
eftChar from to
    | from > to = []
    | otherwise = from : eftChar (succ from) to


splitStr :: Char -> String -> [String]
splitStr _ [] = []
splitStr c s = word : splitStr c rest
    where word = takeWhile (/=c) trimmed
          rest = dropWhile (/=c) trimmed 
          trimmed = dropWhile (==c) s

splitWords = splitStr ' '
splitLines = splitStr '\n'

acro :: String -> String
acro s = [c | c <- s, elem c ['A'..'Z']]

sqrs = [x^2 | x <- [1..5]]
cubes = [x^3 | x <- [1..5]]

tuples = [(x, y) | x <- sqrs, y <- cubes, x < 50 && y < 50]

removeArticles :: String -> String
removeArticles s = concat $ intersperse " " filtered
    where filtered = filter (not . isArticle) (splitWords s)
          articles = ["the", "an", "a"]
          isArticle = flip elem $ articles

myZip :: [a] -> [b] -> [(a, b)]
myZip _ [] = []
myZip [] _ = []
myZip (x:xs) (y:ys) = (x, y) : myZip xs ys

myZipWith :: (a -> b -> c) -> [a] -> [b] -> [c]
myZipWith _ [] _ = []
myZipWith _ _ [] = []
myZipWith f (x:xs) (y:ys) = (f x y) : myZipWith f xs ys

myZip' :: [a] -> [b] -> [(a, b)]
myZip' = myZipWith (,)

myAnd :: [Bool] -> Bool
myAnd [] = True
myAnd (False:_) = False
myAnd (_:xs) = myAnd xs

myOr :: [Bool] -> Bool
myOr [] = False
myOr (True:_) = True
myOr (_:xs) = myOr xs

myAny :: (a -> Bool) -> [a] -> Bool
myAny _ [] = False
myAny f (x:xs)
    | f x = True
    | otherwise = myAny f xs

myElem :: Eq a => a -> [a] -> Bool
myElem e [] = False
myElem e (x:xs)
    | e == x = True
    | otherwise = myElem e xs

myElem' x = myAny (==x)

myReverse :: [a] -> [a]
myReverse [] = []
myReverse (x:xs) = myReverse xs ++ [x]

squish :: [[a]] -> [a]
squish [] = []
squish (x:xs) = x ++ squish xs

squishMap :: (a -> [b]) -> [a] -> [b]
squishMap f xs = squish $ map f xs

squish' = squishMap id

myMaximumBy :: (a -> a -> Ordering) -> [a] -> a
myMaximumBy _ [] = error "empty list"
myMaximumBy _ [x] = x
myMaximumBy cmp (x:y:rest) = case cmp x y of
    LT -> myMaximumBy cmp (y:rest)
    _ -> myMaximumBy cmp (x:rest)

myMinimumBy :: (a -> a -> Ordering) -> [a] -> a
myMinimumBy _ [] = error "empty list"
myMinimumBy _ [x] = x
myMinimumBy cmp (x:y:rest) = case cmp x y of
    GT -> myMinimumBy cmp (y:rest)
    _ -> myMinimumBy cmp (x:rest)

myMaximum :: Ord a => [a] -> a
myMaximum = myMaximumBy compare

myMinimum :: Ord a => [a] -> a
myMinimum = myMinimumBy compare