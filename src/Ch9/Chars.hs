module Ch9.Chars where

import Data.Char (isUpper, toUpper)

uppers :: String -> String
uppers = filter isUpper

capt :: String -> String
capt [] = []
capt (c:cs) = toUpper c : cs

holler :: String -> String
holler [] = []
holler (c:cs) = toUpper c : holler cs

holler' = map toUpper

captFirst :: String -> Char
captFirst (c:cs) = toUpper c

captFirst' s = toUpper $ head s

captFirst'' = toUpper . head