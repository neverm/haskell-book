module Ch9.Cipher where

import Data.Char

alphabetSize = 26

encode :: Int -> String -> String
encode n s = map (encodeChar n) s

decode :: Int -> String -> String
decode n s = map decoder s
    where decoder = encodeChar (-n)

encodeChar :: Int -> Char -> Char
encodeChar n c
    | c >= 'a' && c <= 'z' = encodeWithBase n 'a' c
    | c >= 'A' && c <= 'Z' = encodeWithBase n 'A' c
    | otherwise = c

encodeWithBase :: Int -> Char -> Char -> Char
encodeWithBase n base c = chr $ rotatedPos + ord base
    where rotatedPos = (n + pos) `mod` alphabetSize
          pos = ord c - ord base
