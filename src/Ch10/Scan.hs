module Ch10.Scan where

fibs = 1 : scanl (+) 1 fibs

first20 = take 20 fibs

less100 = takeWhile (< 100) fibs

conseq = scanl (+) 1 (repeat 1)
facts = scanl (*) 1 conseq