module Ch10.Exercises where

stops = "pbtdkg"
vowels = "aeiou"

type Triple = (Char, Char, Char)

combos :: String -> String -> [Triple]
combos ss vs = [(s1, v, s2) | s1 <- ss, s2 <- ss, v <- vs]

startsWith :: Char -> Triple -> Bool
startsWith c (a, _, _) = c == a

startCombos :: String -> String -> [Triple]
startCombos ss vs = filter (startsWith 'p') $ combos ss vs

-- pointfree startCombos for fun

o :: (a -> b) -> (t1 -> t2 -> a) -> t1 -> t2 -> b
o g f = curry $ g . uncurry f

startCombos' :: String -> String -> [Triple]
startCombos' = filter (startsWith 'p') `o` combos


myOr :: [Bool] -> Bool
myOr = foldr (||) False

myAny :: (a -> Bool) -> [a] -> Bool
myAny f = foldr (||) False . map f

myAny' :: (a -> Bool) -> [a] -> Bool
myAny' f = foldr combiner False
    where combiner x acc
            | (f x) = True
            | otherwise = acc

myElem :: Eq a => a -> [a] -> Bool
myElem e = foldr (||) False . map (==e)

myElem' :: Eq a => a -> [a] -> Bool
myElem' e = any (==e)

myReverse :: [a] -> [a]
myReverse = foldl (flip (:)) []

myMap :: (a -> b) -> [a] -> [b]
myMap f = foldr mapper []
    where mapper x acc = (f x) : acc

myFilter :: (a -> Bool) -> [a] -> [a]
myFilter f = foldr combiner []
    where combiner x acc
            | f x = x : acc
            | otherwise = acc

squish :: [[a]] -> [a]
squish = foldr (++) []

squishMap :: (a -> [b]) -> [a] -> [b]
squishMap f = foldr (\x acc -> (f x) ++ acc) []

squishMap' :: (a -> [b]) -> [a] -> [b]
squishMap' f = squish . map f

squish' :: [[a]] -> [a]
squish' = squishMap id

myMaximumBy :: (a -> a -> Ordering) -> [a] -> a
myMaximumBy _ [] = error "nahui"
myMaximumBy cmp (x:xs) = foldr aggr x xs
    where aggr current max = case cmp current max of
            GT -> current
            _ -> max

negateOrdering :: Ordering -> Ordering
negateOrdering GT = LT
negateOrdering LT = GT
negateOrdering EQ = EQ

myMinimumBy :: (a -> a -> Ordering) -> [a] -> a
myMinimumBy cmp = myMaximumBy $ negateOrdering `o` cmp