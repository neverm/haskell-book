module Ch10.Db where

import Data.Time (UTCTime(..), fromGregorian, secondsToDiffTime)

data Item = DbString String
          | DbNumber Integer
          | DbDate   UTCTime
          deriving (Eq, Show, Ord)

theDB :: [Item]
theDB =
    [ DbDate (UTCTime
        (fromGregorian 1911 5 1)
        (secondsToDiffTime 34123))
        , DbNumber 9001
        , DbNumber 5001
        , DbString "Hello, world!"
        , DbDate (UTCTime
        (fromGregorian 1921 5 1)
        (secondsToDiffTime 34123))
    ]

getDate :: Item -> UTCTime
getDate (DbDate d) = d
getDate _ = error "sraka"

isDate :: Item -> Bool
isDate (DbDate _) = True
isDate _ = False

filterDBDate :: [Item] -> [UTCTime]
filterDBDate = map getDate . (filter isDate)

filterDBNumber :: [Item] -> [Integer]
filterDBNumber = map getNumber . (filter isNumber)
    where isNumber record = case record of
            DbNumber n -> True
            _ -> False
          getNumber x = case x of
            DbNumber d -> d
            _ -> error "sraka"
              
mostRecent :: [Item] -> UTCTime
mostRecent = maximum . filterDBDate

sumDB :: [Item] -> Integer
sumDB = sum . filterDBNumber

avgDB :: [Item] -> Double
avgDB db = fromIntegral (sumDB db) / len
    where len = fromIntegral (length $ filterDBNumber db)