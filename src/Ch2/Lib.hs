module Ch2.Lib where

-- expressions

x = 1 + 1

five = 5

-- functions

triple x = x + x + x

-- function application

a = triple 5

-- infix as postfix

b = (+) 1 2

-- postfix as infix

my_add x y = x + y

c = 5 `my_add` 10

-- infix as expression

f = (+)

-- apply operator, has very low precedence
-- use to avoid parentheses

d = (2 -) $ 3 + 4


-- let and where

smth = let x = 5; y = 10 in x * y;

smth2 = x * y
    where x = 5
          y = 6

-- exercises

waxOn = x * 5
    where x = y ^ 2
          y = z + 8
          z = 7