module Ch8.Recursion where

dividedBy :: Integral a => a -> a -> (a, a)
dividedBy num denom = go num denom 0
    where go n d count
            | (n < d) = (count, n)
            | otherwise =
                go (n - d) d (count + 1)

-- exercises
sumToN :: (Eq a, Num a) => a -> a
sumToN n
    | n == 0 = 0
    | otherwise = n + sumToN (n - 1)

myMul :: (Integral a) => a -> a -> a
myMul a b
    | b < 0 = negate $ myMul a (-b)
    | a < 0 = negate $ myMul (-a) b
    | b == 0 = 0
    | otherwise = a + myMul a (b - 1)

data DivisionResult = DivByZero | Result Integer deriving (Show)

divBy :: Integer -> Integer -> DivisionResult
divBy a b
    | b == 0 = DivByZero
    | otherwise = Result $ sign * (go (abs a) (abs b) 0)
        where go n d count
                | (n < d) = count
                | otherwise = go (n - d) d (count + 1)
              sign
                | (a < 0) && (b < 0) = 1
                | (b < 0) = -1
                | (a < 0) = -1
                | otherwise = 1

mc91 :: Integer -> Integer
mc91 n
   | n > 100 = n - 10
   | otherwise = mc91 . mc91 $ (n + 11)

