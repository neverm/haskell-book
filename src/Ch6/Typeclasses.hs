module Ch6.Typeclasses where

import Data.List (sort)

data Trivial = Trivial'

instance Eq Trivial where
    Trivial' == Trivial' = True


data Day =
    Mon | Tue | Wed | Thu | Fri | Sat | Sun deriving Show

data Date = Date Day Int deriving Show

instance Eq Day where
    (==) Mon Mon = True
    (==) Tue Tue = True
    (==) Wed Wed = True
    (==) Thu Thu = True
    (==) Fri Fri = True
    (==) Sat Sat = True
    (==) Sun Sun = True
    (==) _ _ = False

instance Eq Date where
    (==) (Date weekday dayOfMonth)
         (Date weekday' dayOfMonth') =
        weekday == weekday'
        && dayOfMonth == dayOfMonth


data Thing a = Thing a

-- equality with constrained type variable
instance Eq a => Eq (Thing a) where
    (==) (Thing x) (Thing y) = x == y


-- exercises

data TisAnInteger = TisAn Integer

instance Eq TisAnInteger where
    TisAn x == TisAn y = x == y

data TwoIntegers = Two Integer Integer

instance Eq TwoIntegers where
    (==) (Two a b) (Two a' b') =
        a == a' && b == b'

data StringOrInt =
    TisAnInt Int
  | TisAString String

instance Eq StringOrInt where
    (==) (TisAnInt a) (TisAnInt b) = a == b
    (==) (TisAString s) (TisAString s') = s == s'
    (==) _ _ = False

data Pair a = Pair a a

instance Eq a => Eq (Pair a) where
    (==) (Pair a b) (Pair a' b') =
        a == a' && b == b'

data Tuple a b = Tuple a b

instance (Eq a, Eq b) => Eq (Tuple a b) where
    (==) (Tuple a b) (Tuple a' b') =
        a == a' && b == b'

data Which a = ThisOne a | ThatOne a

instance Eq a => Eq (Which a) where
    (==) (ThisOne a) (ThisOne a') = a == a'
    (==) (ThatOne a) (ThatOne a') = a == a'
    (==) _ _ = False

data EitherOr a b = Hello a | Goodbye b

instance (Eq a, Eq b) => Eq (EitherOr a b) where
    (==) (Hello a) (Hello a') = a == a'
    (==) (Goodbye a) (Goodbye a') = a == a'
    (==) _ _ = False


-- own classes

class Numberish a where
    fromNumber :: Integer -> a
    toNumber :: a -> Integer

newtype Age = Age Integer deriving (Show, Eq)

instance Numberish Age where
    fromNumber n = Age n
    toNumber (Age n) = n

newtype Year = Year Integer deriving (Show, Eq)

instance Numberish Year where
    fromNumber n = Year n
    toNumber (Year n) = n

sumNumerish :: Numberish a => a -> a -> a
sumNumerish a b = fromNumber summed
    where intOfA = toNumber a
          intOfB = toNumber b
          summed = intOfA + intOfB

-- exercises

data Mood = Woot | Blah deriving (Show, Eq)

settleDown x = if x == Woot then Blah else x

type Subject = String
type Verb = String
type Object = String
data Sentence =
    Sentence Subject Verb Object 
    deriving (Eq, Show)
s1 = Sentence "dogs" "drool"
s2 = Sentence "Julie" "loves" "dogs"


f :: RealFrac a => a
f = 1.0

e :: a
e = undefined

freud :: Ord a => a -> a
freud x = x

freud' :: Int -> Int
freud' x = x

myX = 1 :: Int

sigmund' :: Int -> Int
sigmund' x = myX

jung :: [Int] -> Int
jung xs = head (sort xs)

young :: Ord a => [a] -> a
young xs = head (sort xs)

mySort :: [Char] -> [Char]
mySort = sort

signifier :: [Char] -> Char
signifier xs = head (mySort xs)

-- type kwon do

chk :: Eq b => (a -> b) -> a -> b -> Bool
chk f a b = (f a) == b

arith :: Num b
      => (a -> b)
      -> Integer
      -> a
      -> b
arith f n a = (f a) + (fromInteger n)


-- examples

class Cls a where
    someAction :: a -> a

-- class inheritence
class Cls a => AnotheCls a where
    otherAction :: a -> a