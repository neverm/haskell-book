{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE FlexibleInstances #-}

module Ch11.Adt where

import Data.Char (toUpper)
import Data.List (intercalate)

-- examples
data SingleT = SingleC

data PhantomT a = PhantomC

smth = SingleC :: SingleT

p1 :: PhantomT a
p1 = PhantomC

p2 :: Num a => PhantomT a
p2 = PhantomC

p3 :: PhantomT [[[[[Int]]]]]
p3 = PhantomC


data Price = Price Integer deriving (Eq, Show)

type Size = Integer

data Manufacturer = Mini | Mazda | Tata deriving (Eq, Show)

data Airline = PapuAir | TakeYourChancesUnited | Catapults deriving (Eq, Show)

data Vehicle = Car Manufacturer Price | Plane Airline Size
--    [1]      [2]      [3]       [4]    [5]    [6]    [7]
-- 1 type constructor
-- 2, 5 - data constructors of type Vehicle
-- 3, 4, 6, 7 - type arguments to data constructors

-- exercises

myCar = Car Mini (Price 2000)
urCar = Car Mazda (Price 1000)
clownCar = Car Tata (Price 100)
doge = Plane PapuAir

isCar :: Vehicle -> Bool
isCar (Car _ _) = True
isCar _ = False

isPlane :: Vehicle -> Bool
isPlane (Plane _ _) = True
isPlane _ = False

areCars :: [Vehicle] -> Bool
areCars = all isCar

getManu :: Vehicle -> Manufacturer
getManu (Car m _) = m

-- examples

newtype Goats = Goats Int deriving (Eq, Show)

-- language extension allows deriving typeclasses that underlying type (Int here)
-- has instance of
newtype Cows = Cows Int deriving (Eq, Show, TooMany) 

class TooMany a where
  tooMany :: a -> Bool

instance TooMany Int where
  tooMany n = n > 13

instance TooMany Goats where
  tooMany (Goats n) = n > 42

-- exercises

newtype SomeTuple = SomeTuple (Int, String) deriving (Eq, Show)
instance TooMany SomeTuple where
  tooMany (SomeTuple (n, s)) = n > 50 || length s > 7

-- requires FlexibleInstances
instance TooMany (Int, String) where
  tooMany (n, s) = n > 50 || length s > 7

newtype Field = Field (Int, Int) deriving (Eq, Show)
instance TooMany Field where
  tooMany (Field (n, m)) = (n + m) > 10

-- requires FlexibleInstances
instance TooMany (Int, Int) where
  tooMany (n, m) = (n + m) > 10

instance (Num a, TooMany a) => TooMany (a, a) where
  tooMany (a, b) = tooMany (a + b)


-- examples

data QuantumBool = QTrue
                 | QFalse
                 | QBoth

data TwoQs = MkTwoQs QuantumBool QuantumBool


data Person
  = Person { name :: String
           , age :: Int }
           deriving (Eq, Show)

-- examples

data Fiction = Fiction deriving Show
data Nonfiction = Nonfiction deriving Show

data BookType = FictionBook Fiction
              | NonfictionBook Nonfiction
              deriving Show

type AuthorName = String

-- Not in the normal form: a product type with sum type as arg
data Author = Author (AuthorName, BookType)

-- In normal form: the type is reduced to be a sum type of products
-- notice that we got rid of "BookType" type
data Author2 = 
    Fiction2 AuthorName
  | Nonfiction2 AuthorName


-- exercises

data FlowerType = Gardenia
                | Daisy
                | Rose
                | Lilac
                deriving Show

type Gardener = String

data Garden =
    Garden Gardener FlowerType
    deriving Show


data GardenNormal = GardenGardenia Gardener
                  | GardenDaisy Gardener
                  | GardenRose Gardener
                  | GardenLilac Gardener


-- examples

data GuessWhat = ChickenButt deriving (Eq, Show)

data Id a = MkId a deriving (Eq, Show)

data Product a b = Product a b deriving (Eq, Show)

data Sum a b =
    First a
  | Second b
  deriving (Eq, Show)

data RecordProduct a b =
  RecordProduct { pfirst :: a
                , psecond :: b }
                deriving (Eq, Show)


newtype NumCow = NumCow Int deriving (Eq, Show)

newtype NumPig = NumPig Int deriving (Eq, Show)

data Farmhouse = Farmhouse NumCow NumPig deriving (Eq, Show)

-- just an alias, can use Product NumCow NumPig directly
type Farmhouse' = Product NumCow NumPig

newtype NumSheep = NumSheep Int deriving (Eq, Show)

data BigFarmhouse = BigFarmhouse NumCow NumPig NumSheep deriving (Eq, Show)

type BigFarmhouse' = Product NumCow (Product NumPig NumSheep)


type Name = String

type Age = Int

type LovesMud = Bool

type PoundsOfWhool = Int

data CowInfo = CowInfo Name Age  deriving (Eq, Show)

data PigInfo = PigInfo Name Age LovesMud  deriving (Eq, Show)

data SheepInfo = SheepInfo Name Age PoundsOfWhool  deriving (Eq, Show)

data Animal =
    Cow CowInfo
  | Pig PigInfo
  | Sheep SheepInfo

-- alias again, can use Sum CowInfo (Sum PigInfo SheepInfo) in signatures
type Animal' = Sum CowInfo (Sum PigInfo SheepInfo)

-- valid values for Animal' are
-- First (CowInfo ...)
-- Second (First (PigInfo ...))
-- Second (Second (SheepInfo ...))
-- any combination of First and Second is allowed, like First (First (CowInfo ...))
-- but those cannot pass as Animal' and won't typecheck


trivialValue :: GuessWhat
trivialValue = ChickenButt


idInt :: Id Integer
idInt = MkId 10


person :: Product String Bool
person = Product "name" True

data Twitter = Twitter deriving (Eq, Show)

data AskFm = AskFm deriving (Eq, Show)

socialNetwork :: Sum Twitter AskFm
socialNetwork = First Twitter


data OS = Linux
        | Mac
        | Windows
        | BSD
        deriving (Eq, Show)

data ProgLang = Haskell
              | Agda
              | Idris
              | Go
              deriving (Eq, Show)

data Programmer =
  Programmer { os :: OS
             , lang :: ProgLang }
  deriving (Eq, Show)

-- exercises

allOperatingSystems :: [OS]
allOperatingSystems =
  [ Linux
  , BSD
  , Mac
  , Windows
  ]
allLanguages :: [ProgLang]
allLanguages =
  [Haskell, Agda, Idris, Go]
allProgrammers :: [Programmer]
allProgrammers = concat $ map genProgs allOperatingSystems where
  genProgs os = map (Programmer os) allLanguages


-- examples

data BinaryTree a =
    Leaf
  | Node (BinaryTree a) a (BinaryTree a)
  deriving (Eq, Show)

insert' :: Ord a => a -> BinaryTree a -> BinaryTree a
insert' x Leaf = Node Leaf x Leaf
insert' x t@(Node left a right)
  | x == a = t
  | x < a = Node (insert' x left) a right
  | x > a = Node left a (insert' x right)

-- exercises

mapt :: (a -> b) -> BinaryTree a -> BinaryTree b
mapt _ Leaf = Leaf
mapt f (Node left a right) =
  Node (mapt f left) (f a) (mapt f right)

preorder :: BinaryTree a -> [a]
preorder Leaf = []
preorder (Node left a right) = a : preorder left ++ preorder right

inorder :: BinaryTree a -> [a]
inorder Leaf = []
inorder (Node left a right) = inorder left ++ (a : inorder right)

postorder :: BinaryTree a -> [a]
postorder Leaf = []
postorder (Node left a right) = postorder left ++ postorder right ++ [a]

testTree :: BinaryTree Integer
testTree =
  Node (Node Leaf 1 Leaf)
  2
  (Node Leaf 3 Leaf)

foldt :: (a -> b -> b) -> b -> BinaryTree a -> b
foldt _ acc Leaf = acc
foldt f acc (Node left a right) = foldt f leftF right where
  leftF = foldt f current left
  current = (f a acc)


isSubseqOf :: (Eq a) => [a] -> [a] -> Bool
isSubseqOf [] _ = True
isSubseqOf _ [] = False
isSubseqOf as bs = match as bs where
  -- awful backtracking but it works
  match [] _ = True
  match _ [] = False
  match xs ys@(y:ys') = case try xs ys of
    True -> True
    False -> try xs ys'
  try [] _ = True
  try _ [] = False
  try (x:xs) (y:ys)
    | x == y = try xs ys
    | otherwise = try as ys

capitalizeWords :: String -> [(String, String)]
capitalizeWords s = go s "" where
  go "" "" = []
  go "" consumed = [mkpair $ reverse consumed]
  go (' ':cs) "" = go cs ""
  go (' ':cs) consumed = mkpair (reverse consumed) : go cs ""
  go (c:cs) consumed = go cs (c:consumed)

  mkpair s@(c:cs) = (s, toUpper c : cs)

capitalizeWord :: String -> String
capitalizeWord "" = ""
capitalizeWord (c:cs) = toUpper c : cs

splitWords :: String -> [String]
splitWords text = go text "" where
  go "" current = [reverse current]
  go (c:cs) current
    | c == ' ' = reverse current : go cs ""
    | otherwise = go cs (c:current)

capitalizeWords' s = zip words $ map capitalizeWord words where
  words = splitWords s

splitParagraphs :: String -> [String]
splitParagraphs text = go text "" where
  go :: String -> String -> [String]
  go "" currentWord = [reverse currentWord]
  go (c:c':cs) currentWord
    | c == '.' && c' == ' ' = reverse currentWord : go cs ""
    | otherwise = go (c':cs) (c:currentWord)
  go (c:cs) currentWord = go "" (c:currentWord)

capitalizeParagraph :: String -> String
capitalizeParagraph = intercalate ". " . map capitalizeWord . splitParagraphs