module Ch11.Vigenere where

import Data.Char (ord, chr)

alphabetSize = 26

encode :: String -> String -> String
encode codeWord s = zipWith encodeNth s $ makeCodeIdx codeWord s

-- make index list of character positions in the codeWord
makeCodeIdx :: String -> String -> [Char]
makeCodeIdx _ "" = []
makeCodeIdx codeWord s = map snd $ drop 1 $ scanl makeIdx (0, '\0') s where
    makeIdx (counter, current) c
        | c == ' ' = (counter, c)
        | otherwise = (counter+1, wrappedAt codeWord $ counter)


encode' :: String -> String -> String
encode' codeWord toEncode = reverse $ snd $ foldr (combine codeWord) (0, "") $ reverse toEncode where

combine :: String -> Char -> (Int, String) -> (Int, String)
combine codeWord c (idx, encoded)
    | shouldEncode c = let e = encodeNth c (wrappedAt codeWord idx) in
        (idx+1, e : encoded)
    | otherwise = (idx, c : encoded)
    -- combine c (idx, encoded) = (idx, encoded)

-- the best try, really
encode'' :: String -> String -> String
encode'' codeWord toEncode = go toEncode 0 where
    go "" _ = ""
    go (c:rest) idx
        | shouldEncode c = let e = encodeNth c (wrappedAt codeWord idx) in
            e : (go rest (idx+1))
        | otherwise = c : (go rest idx)


decode :: String -> String -> String
decode codeWord toDecode = encode (map invChar codeWord) toDecode where
    invChar c = case getBase c of
        Nothing -> c
        Just base -> encodeChar base c (chr base) True

wrappedAt :: [a] -> Int -> a
wrappedAt [] _ = error "zero length list"
wrappedAt xs i = xs !! w where
    w = i `mod` length xs

encodeNth :: Char -> Char -> Char
encodeNth toEncode codeC = case getBase toEncode of
    Nothing -> toEncode
    Just base -> encodeChar base codeC toEncode False

encodeChar :: Int -> Char -> Char -> Bool -> Char
encodeChar base codeC toEncode neg = chr pos where
    posEnc = posFromChar base toEncode
    sign = if neg then -1 else 1
    posCode = sign * posFromChar base codeC
    pos = base + (addPos posEnc posCode)
        
type AlphabetPos = Int

posFromChar :: Int -> Char -> AlphabetPos
posFromChar base c = (ord c) - base

-- get alphabet position of two alphabet positions added together
-- results that are over alphabetSize are wrapped around
addPos :: AlphabetPos -> AlphabetPos -> AlphabetPos
addPos a b = (a + b) `mod` alphabetSize

shouldEncode :: Char -> Bool
shouldEncode c = case getBase c of
    Nothing -> False
    _ -> True
    
getBase :: Char -> Maybe Int
getBase c
    | c >= 'a' && c <= 'z' = Just (ord 'a')
    | c >= 'A' && c <= 'Z' = Just (ord 'A')
    | otherwise = Nothing