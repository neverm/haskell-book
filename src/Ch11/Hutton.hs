module Ch11.Hutton where

data Expr
    = Lit Integer
    | Add Expr Expr

eval :: Expr -> Integer
eval (Lit i) = i
eval (Add a b) = eval a + eval b

printExpr :: Expr -> String
printExpr = show

instance Show Expr where
    show (Lit i) = show i
    show (Add a b) = show a ++ " + " ++ show b