module Ch12.Nats where

data Nat = Zero | Succ Nat deriving (Eq, Show)

natToInteger :: Nat -> Integer
natToInteger Zero = 0
natToInteger (Succ nat) = 1 + natToInteger nat

integerToNat :: Integer -> Maybe Nat
integerToNat i
    | i < 0 = Nothing
    | otherwise = Just $ inner i where
        inner :: Integer -> Nat
        inner n
            | n == 0 = Zero
            | otherwise = Succ $ inner (n-1)
