module Ch12.Maybe where

isJust :: Maybe a -> Bool
isJust (Just _) = True
isJust _ = False

isNothing :: Maybe a -> Bool
isNothing (Just _) = False
isNothing _ = True

mayybe :: b -> (a -> b) -> Maybe a -> b
mayybe _ f (Just val) = f val
mayybe def f Nothing = def

fromMaybe :: a -> Maybe a -> a
fromMaybe def (Just v) = v
fromMaybe def _ = def

listToMaybe :: [a] -> Maybe a
listToMaybe (x:xs) = Just x
listToMaybe [] = Nothing

maybeToList :: Maybe a -> [a]
maybeToList Nothing = []
maybeToList (Just v) = [v]

catMaybes :: [Maybe a] -> [a]
catMaybes [] = []
catMaybes (x:xs) = case x of
    Nothing -> catMaybes xs
    Just v -> v : catMaybes xs

flipMaybe :: [Maybe a] -> Maybe [a]
flipMaybe [] = Just []
flipMaybe (x:xs) = case x of
    Nothing -> Nothing
    Just v -> case flipMaybe xs of
        Nothing -> Nothing
        Just vs -> Just (v:vs)