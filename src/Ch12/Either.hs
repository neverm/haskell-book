module Ch12.Either where

lefts' :: [Either a b] -> [a]
lefts' = foldr combine [] where
    combine :: Either a b -> [a] -> [a]
    combine (Left l) ls = l : ls
    combine (Right _) ls = ls

right' :: [Either a b] -> [b]
right' = foldr combine [] where
    combine :: Either a b -> [b] -> [b]
    combine (Right r) rs = r : rs
    combine (Left _) rs = rs

partitionEithers' :: [Either a b] -> ([a], [b])
partitionEithers' [] = ([], [])
partitionEithers' (e:es) = let (ls, rs) = partitionEithers' es in
    case e of
        Left l -> (l:ls, rs)
        Right r -> (ls, r:rs)

eitherMaybe' :: (b -> c) -> Either a b -> Maybe c
eitherMaybe' f (Left _) = Nothing
eitherMaybe' f (Right r) = Just $ f r

either' :: (a -> c) -> (b -> c) -> Either a b -> c
either' f _ (Left l) = f l
either' _ g (Right r) = g r

eitherMaybe'' :: (b -> c) -> Either a b -> Maybe c
eitherMaybe'' f = either' (const Nothing) (Just . f)