module Ch12.Person where

type Name = String
type Age = Integer

data Person = Person Name Age deriving (Show)

data PersonError = NameEmpty | AgeTooLow deriving (Eq, Show)

type Validated a = Either [PersonError] a

ageOkay :: Age -> Validated Age
ageOkay age
  | age >= 0 = Right age
  | otherwise = Left [AgeTooLow]

nameOkay :: Name -> Validated Name
nameOkay name
  | name == "" = Left [NameEmpty]
  | otherwise = Right name

mkPerson :: Name -> Age -> Validated Person
mkPerson name age =
    mkPerson' (nameOkay name) (ageOkay age) where
    mkPerson' :: Validated Name -> Validated Age -> Validated Person
    mkPerson' (Right nameOk) (Right ageOk) = Right $ Person nameOk ageOk
    mkPerson' (Left nameErrors) (Left ageErrors) = Left $ nameErrors ++ ageErrors
    mkPerson' (Left nameErrors) _ = Left nameErrors
    mkPerson' _ (Left ageErrors) = Left ageErrors
