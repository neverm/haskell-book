module Ch12.StringProc where

import Ch11.Adt (splitWords)
import Data.List (intercalate)

notThe :: String -> Maybe String
notThe s
  | s == "the" = Nothing
  | otherwise = Just s

replaceThe :: String -> String
replaceThe s = intercalate " " $ map repl $ splitWords s where
    repl s = case notThe s of
        Nothing -> "a"
        Just s -> s

isVowel :: Char -> Bool
isVowel c = any (==c) "aouei"

countTheBeforeVowel :: String -> Integer
countTheBeforeVowel s = consume 0 $ splitWords s where
    consume acc (tip:next@(c:cs):rest)
        | tip == "the" && isVowel c = consume (acc + 1) rest
        | otherwise = consume acc (next:rest)
    consume acc (w:rest) = consume acc rest
    consume acc [] = acc

countVowels :: String -> Integer
countVowels = fromIntegral . length . filter isVowel

newtype Word' = Word' String deriving (Eq, Show)

mkWord :: String -> Maybe Word'
mkWord s
  | (countVowels s) * 2 > (fromIntegral $ length s) = Nothing
  | otherwise = Just $ Word' s