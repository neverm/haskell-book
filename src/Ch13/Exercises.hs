module Ch13.Exercises where

import Control.Monad (forever)
import System.Exit (exitSuccess)
import Data.Char (toLower)
import Data.List (intersperse)

import Ch12.Person
import Ch11.Vigenere (encode)

runEncode :: IO ()
runEncode = forever $ do
    putStr "Enter string to encode: "
    line <- getLine
    putStrLn $ "Encoded: " ++ encode "some_codeword" line

palindrome :: IO ()
palindrome = forever $ do
    line <- getLine
    case (line == reverse line) of
        True -> putStrLn "It's a palindrome!"
        False -> do
            putStrLn "Nope"
            exitSuccess

isPalindrome :: String -> Bool
isPalindrome s = clean == reverse clean where
    clean = map toLower $ filter (/=' ') s

gimmePerson :: IO ()
gimmePerson = forever $ do
    putStr "Enter name: "
    name <- getLine
    putStrLn "Enter age: "
    ageStr <- getLine
    let age = read ageStr :: Integer in
        case mkPerson name age of
            Left errs -> putStrLn $ showsToStr errs
            Right p -> putStrLn $ "Good job, " ++ show p

showsToStr :: Show a => [a] -> String
showsToStr as = concat $ intersperse " " $ map show as