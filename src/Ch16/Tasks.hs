{-# LANGUAGE FlexibleInstances #-}
module Ch16.Tasks where

import Test.QuickCheck

a = fmap (+1) $ read "[1]" :: [Int]

b = (fmap . fmap) (++ "lol") (Just ["Hi,", "Hello"])

c = fmap (*2) (\x -> x - 2)

d = fmap ((return '1' ++) . show) (\x -> [x, 1..3])

e :: IO Integer
e = let ioi = readIO "1" :: IO Integer
        changed = fmap (("123"++) . show) ioi
    in fmap ((*3) . read) changed

-- instances

newtype Identity a = Identity a deriving (Eq, Show)

instance Arbitrary a => Arbitrary (Identity a) where
  arbitrary = do
    a <- arbitrary
    return (Identity a)

instance Functor Identity where
  fmap f (Identity a) = Identity (f a)

data Pair a = Pair a a deriving (Eq, Show)

instance Arbitrary a => Arbitrary (Pair a) where
  arbitrary = do
    a1 <- arbitrary
    a2 <- arbitrary
    return (Pair a1 a2)

instance Functor Pair where
  fmap f (Pair a a') = Pair (f a) (f a')

data Two a b = Two a b deriving (Eq, Show)

instance (Arbitrary a, Arbitrary b) => Arbitrary (Two a b) where
  arbitrary = do
    a <- arbitrary
    b <- arbitrary
    return (Two a b)

-- to make an example, the first type in Two is t here, although
-- conventionally it would be called a, just like the argument
instance Functor (Two t) where
  -- we match on data constructor, and a here has type t. We don't know
  -- anything about this type, so we can only return the value back

  -- functor instance expects a type of kind * -> *, which is a type that expects
  -- another type as argument. That argument is used in fmap signature, and we make
  -- use of it in fmap body
  fmap f (Two a b) = Two a (f b)

  -- in other words, a functor instance says: there is an instance of functor for
  -- some type that wraps another type. I don't care how many of types it wraps,
  -- but I will only use one, so please provide me a type that expects another kind.
  -- The value of that type I will use with my mapping function

data Three a b c = Three a b c deriving (Eq, Show)

instance (Arbitrary a, Arbitrary b, Arbitrary c) => Arbitrary (Three a b c) where
  arbitrary = do
    a <- arbitrary
    b <- arbitrary
    c <- arbitrary
    return (Three a b c)

instance Functor (Three a b) where
  fmap f (Three a b c) = Three a b (f c)


data Three' a b = Three' a b b deriving (Eq, Show)

instance (Arbitrary a, Arbitrary b) => Arbitrary (Three' a b) where
  arbitrary = do
    a <- arbitrary
    b <- arbitrary
    b' <- arbitrary
    return (Three' a b b')

instance Functor (Three' a) where
  fmap f (Three' a b b') = Three' a (f b) (f b')

data Four a b c d = Four a b c d

-- boring because the same as three, just map over d and leave other fields alone
-- not doing

data Four' a b = Four' a a a b deriving (Eq, Show)

instance (Arbitrary a, Arbitrary b) => Arbitrary (Four' a b) where
  arbitrary = do
    a <- arbitrary
    a' <- arbitrary
    a'' <- arbitrary
    b <- arbitrary
    return (Four' a a' a'' b)

instance Functor (Four' a) where
  fmap f (Four' a a' a'' b) = Four' a a' a'' (f b)

-- Cannot implement functor for this type, since it has kind * and we need
-- kind * -> *
data Trivial = Trivial


-- Exercise: Possibly

data Possibly a = Nah | Meh a deriving (Eq, Show)

instance Functor Possibly where
  fmap _ Nah = Nah
  fmap f (Meh a) = Meh (f a)

data Sum a b = F a | S b deriving (Eq, Show)

instance Functor (Sum a) where
  fmap _ (F a) = F a
  fmap f (S b) = S (f b)

newtype Constant a b = 
  Constant { getConstant :: a}
  deriving (Eq, Show)

instance Functor (Constant m) where
  -- not sure what v here is, since data constructor has only one member,
  -- it must be the type m (or a in the definition of Constant)
  -- since the function we are passed maps over b, we cannot use it here
  -- and we cannot match in such a way that we get type b. There is no data constructor
  -- using b. It is a phantom type
  fmap _ (Constant v) = Constant v


data Wrap f a = Wrap (f a) deriving (Eq, Show)

instance Functor f => Functor (Wrap f) where
  fmap f (Wrap fa) = Wrap (fmap f fa)


-- final tasks

data Quant a b = Finance | Desk a | Bloor b deriving (Eq, Show)

instance Functor (Quant a) where
  fmap _ Finance = Finance
  fmap _ (Desk a) = Desk a
  fmap f (Bloor b) = Bloor (f b)

data K a b = K a

instance Functor (K a) where
  fmap _ (K b) = K b

newtype Flip f a b =
  Flip (f b a)
  deriving (Eq, Show)

newtype K' a b = K' a

instance Functor (Flip K' a) where
  fmap f (Flip (K' a)) =
    Flip $ K' (f a)

data EvilConst a b = EvilConst b

instance Functor (EvilConst a) where
  fmap f (EvilConst b) = EvilConst (f b)

-- The type here takes apart the structure and underlying data types
-- this way we can refer to the structure (f) separately
-- we could have used just a type a instead of f a, but this way
-- 1. any type could be used, while now f is forced to have (* -> *) kind
-- 2. we can use f later. For example, we can restrict f to some typeclass
-- like it is done below

-- In data constructor (f a) signifies that the member value will have type
-- of f applied to a, that is, structure f with underlying type a

-- when we create values with this data constructor, the type will be inferred
-- from the data passed. If we pass a primitive type, like LiftItOut 42, then
-- f variable in the type will be unititialized
data LiftItOut f a = LiftItOut (f a)

instance Functor f => Functor (LiftItOut f) where
  fmap fun (LiftItOut fa) = LiftItOut (fmap fun fa)

data Parappa f g a = DaWrappa (f a) (g a)

instance (Functor f, Functor g) => Functor (Parappa f g) where
  fmap fun (DaWrappa fa ga) = DaWrappa (fmap fun fa) (fmap fun ga)

data IgnoreOne f g a b =
  IgnoreSomething (f a) (g b)

instance Functor g => Functor (IgnoreOne f g a) where
  fmap fun (IgnoreSomething fa gb) = IgnoreSomething fa (fmap fun gb)

data Notorious g o a t =
  Notorious (g o) (g a) (g t)

instance Functor g => Functor (Notorious g o a) where
  fmap fun (Notorious go ga gt) = Notorious go ga (fmap fun gt)

data List a = Nil | Cons a (List a)

instance Functor List where
  fmap _ Nil = Nil
  fmap f (Cons a rest) = Cons (f a) (fmap f rest)

data GoatLord a
  = NoGoat
  | OneGoat a
  | MoreGoats (GoatLord a)
              (GoatLord a)
              (GoatLord a)

instance Functor GoatLord where
  fmap _ NoGoat = NoGoat
  fmap f (OneGoat a) = OneGoat (f a)
  fmap f (MoreGoats l m r) = MoreGoats l' m' r' where
    l' = fmap f l
    r' = fmap f r
    m' = fmap f m

data TalkToMe a
  = Halt
  | Print String a
  | Read (String -> a)

instance Functor TalkToMe where
  fmap _ Halt = Halt
  fmap f (Print s a) = Print s (f a)
  fmap f (Read reader) = Read (f . reader)

-- test that it works
r = read :: (String -> Int)
rd = Read r
rd' = fmap (+5) rd

applyTalk :: String -> TalkToMe a -> a
applyTalk _ Halt = error "cyka"
applyTalk _ (Print _ a) = a
applyTalk s (Read reader) = reader s