{-# LANGUAGE FlexibleInstances #-}
module Ch16.FlipFunctor where

newtype Flip f a b =
  Flip (f b a)
  deriving (Eq, Show)

data Tuple a b = Tuple a b deriving(Eq, Show)

-- this functor is defined for (Flip Tuple a) type, and cannot be used
-- for tuples in general. So, this is not a flipped functor for all tuples
instance Functor (Flip Tuple a) where
  fmap f (Flip (Tuple a b)) =
    Flip $ Tuple (f a) b