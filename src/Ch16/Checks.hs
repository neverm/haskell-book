module Ch16.Checks where

-- functorIdentity takes a functor and uses id function to check
-- if fmap doesn't touch the structure
functorIdentity :: (Functor f, Eq (f a)) => f a -> Bool
functorIdentity f = fmap id f == f

-- functorCompose takes two functions f and g and a functor x and checks whether
-- applying fmap of (g . f) to x is the same as applying
-- fmap f to x and them applying fmap g to the result
functorCompose :: (Eq (f c), Functor f)
               => (a -> b)
               -> (b -> c)
               -> f a
               -> Bool
functorCompose f g x =
  (fmap g (fmap f x)) == (fmap (g . f) x)
                  
