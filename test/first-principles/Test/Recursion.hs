module Test.Recursion where

import Test.Hspec

import Ch8.Recursion (myMul)

spec :: Spec
spec =
    describe "Hand written multiplication" $ do
        it "2 * 2 = 4" $ do
            myMul 2 2 `shouldBe` 4
        it "2 * 1 = 2" $ do
            myMul 2 1 `shouldBe` 2
        it "2 * 0 = 0" $ do
            myMul 2 0 `shouldBe` 0
        it "2 * -1 = -2" $ do
            myMul 2 (-1) `shouldBe` (-2)
