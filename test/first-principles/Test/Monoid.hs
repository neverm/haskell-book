module Test.Monoid where

import Test.QuickCheck
import Ch15.Assoc
import Ch15.Opt
import Ch15.Tasks
import Data.Monoid (Sum)

runMonoidChecks :: IO ()
runMonoidChecks = do
  putStrLn "Running monoid tests"
  let ma = monoidAssoc
      mli = monoidLeftId
      mri = monoidRightId
  quickCheck (ma :: Bull -> Bull -> Bull -> Bool)
  quickCheck (mli :: Bull -> Bool)
  quickCheck (mri :: Bull -> Bool)

  quickCheck (monoidAssoc :: First' String -> First' String -> First' String -> Bool)
  quickCheck (monoidLeftId :: First' String -> Bool)
  quickCheck (monoidRightId :: First' String -> Bool)

type TwoStrings = Two String String
type ThreeStrings = Three String String String

runSemigroupTask :: IO ()
runSemigroupTask = do
  putStrLn "Running semigroup task tests"
  quickCheck (semigroupAssoc :: Trivial -> Trivial -> Trivial -> Bool)
  quickCheck (semigroupAssoc :: Identity String -> Identity String -> Identity String -> Bool)
  quickCheck (semigroupAssoc :: TwoStrings -> TwoStrings -> TwoStrings -> Bool)
  quickCheck (semigroupAssoc :: ThreeStrings -> ThreeStrings -> ThreeStrings -> Bool)
  quickCheck (semigroupAssoc :: BoolConj -> BoolConj -> BoolConj -> Bool)
  quickCheck (semigroupAssoc :: BoolDisj -> BoolDisj -> BoolDisj -> Bool)
  quickCheck (semigroupAssoc :: Or String Bool -> Or String Bool -> Or String Bool -> Bool)
