module Test.Functor where

import Ch16.Checks
import qualified Ch16.Tasks as Ts
import Test.QuickCheck
import Test.QuickCheck.Function

functorCompose' :: (Eq (f c), Functor f) =>
                     f a
                  -> Fun a b
                  -> Fun b c
                  -> Bool
functorCompose' x (Fun _ f) (Fun _ g) =
  (fmap (g . f) x) == (fmap g . fmap f $ x)

type IntToInt = Fun Int Int

type IntFC = [Int] -> IntToInt -> IntToInt -> Bool

runFunctorExamples :: IO ()
runFunctorExamples = do
  putStrLn "Running functor chapter examples"
  -- create a functorIdentity synonym with a more narrow type that 
  -- quickcheck can use. This function only takes [Int], not any functor
  let f x = functorIdentity (x :: [Int])

  let c = functorCompose (+1) (*2)
  -- same as before, create a more narrow version of functorCompose that
  -- only operates on [Int], to help quickCheck generating instances
  let li x = c (x :: [Int])

  quickCheck f
  quickCheck li
  quickCheck (functorCompose' :: IntFC)

runFunctorExercises :: IO ()
runFunctorExercises = do
  putStrLn "Running functor chapter exercises"

  let i1 x = functorIdentity (x :: Ts.Identity Int)
  let c1 x = functorCompose (+1) (*2) (x :: Ts.Identity Int)
  quickCheck i1
  quickCheck c1

  let i2 x = functorIdentity (x :: Ts.Pair Int)
  let c2 x = functorCompose (+1) (*2) (x :: Ts.Pair Int)
  quickCheck i2
  quickCheck c2

  let i3 x = functorIdentity (x :: Ts.Two String Int)
  let c3 x = functorCompose (+1) (*2) (x :: Ts.Two String Int)
  quickCheck i3
  quickCheck c3

  let i4 x = functorIdentity (x :: Ts.Three String Int Int)
  let c4 x = functorCompose (+1) (*2) (x :: Ts.Three String Int Int)
  quickCheck i4
  quickCheck c4

  let i5 x = functorIdentity (x :: Ts.Three' String Int)
  let c5 x = functorCompose (+1) (*2) (x :: Ts.Three' String Int)
  quickCheck i5
  quickCheck c5

  let i6 x = functorIdentity (x :: Ts.Four' String Int)
  let c6 x = functorCompose (+1) (*2) (x :: Ts.Four' String Int)
  quickCheck i6
  quickCheck c6