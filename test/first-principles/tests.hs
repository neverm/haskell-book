module Main where

import Test.Hspec

import qualified Test.Arithmetic as Arithmetic
import qualified Test.Recursion as Recursion
import qualified Test.Generators as Gens
import Test.Functor (runFunctorExamples, runFunctorExercises)
import Test.Monoid (runMonoidChecks, runSemigroupTask)

runSpecs :: IO ()
runSpecs = hspec $ do
    Arithmetic.spec
    Recursion.spec

main :: IO ()
main = do
  runSpecs
  runMonoidChecks
  runSemigroupTask
  runFunctorExamples
  runFunctorExercises