module Test.Arithmetic where
    
import Test.Hspec
import Test.QuickCheck

spec :: Spec
spec =
    -- describe "Addition" $ do
    --     it "1 + 1 is bigger than 1" $ do
    --         1 + 1 > 1 `shouldBe` True

    describe "Addition property" $ do
        it "x + 1 is always greater \
           \than x" $ do
               property $ \x -> x + 1 > (x :: Int)
